'''
Created on 01.03.2020

@author: jotty
'''
from flask import Flask, session, escape, request

app = Flask(__name__)

@app.route('/')
def toLogin():
    f = open("login.html", "r")
    input_form = f.read()
    f.close()
    
    return fill("campy.html", "Please login", input_form)

@app.route('/login', methods=['GET', 'POST'])
def doLogin():
    if request.method == 'POST':
        name = request.form('username')
        pwd = request.form('password')
        prefs = login(name, pwd)
        if prefs == False:
            msg = f'Sorry, user {escape(name)} is not registered'
        else:
            msg = f'Welcome, {escape(name)}'
        # TODO: result as json object
    else:
        msg = f'Do not use GET for login'
    return msg


def fill(filename, navigation, main):    
    f = open(filename, "r")
    html = f.read()
    f.close()
    
    resp_body = html % {
        'navigation': navigation.replace('\n', ''),
        'main': main.replace('\n', '')
    }

    resp_body = resp_body.encode()
    
    return resp_body

def login(name, pwd):
    res = False
    # TODO: replace by database check
    if name == 'jotty':
        if pwd == 'jotty':
            res = True
    return res

# only on development systems
if __name__ == '__main__':
    app.run()
    