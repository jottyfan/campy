import logging
from cgi import escape
from urllib.parse import parse_qs
from traceback import format_exception

logging.basicConfig(filename='/var/www/html/log/python.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s %(name)s: %(message)s')

def application(env, startResponse):
    try:
        req_body_size = int(env.get('CONTENT_LENGTH', 0))
    except (ValueError):
        req_body_size = 0

    try:
        logging.debug('req_body_size=%s', req_body_size)

        req_body = env['wsgi.input'].read(req_body_size)

        logging.debug('req_body=%s', req_body)

        query_string = env["QUERY_STRING"]

        # in Python2 war es d = urlparse.parse_qs(query_string)
        d = parse_qs(req_body)
        logging.debug('d=%s', d)

        # what is wrong here?
        val = d.get(b'val', [b''])[0].decode('utf-8')
        val = escape(val)

        logging.debug('val=%s', val)

        f = open("/var/www/html/python_form.html", "r")
        html = f.read()
        f.close()

        resp_body = html % {
	    'val': val or ''
        }

        resp_body = resp_body.encode()
        resp_headers = [
		('Content-Type', 'text/html'),
		('Content-Length', str(len(resp_body)))
        ]

        startResponse("200 OK", resp_headers)
        return [resp_body]

    except Exception as exc:
        logging.debug(format_exception(type(exc), exc, exc.__traceback__))
